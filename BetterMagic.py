#!/usr/bin/env python3
# This program generates magical unicorn names!

import random
import subprocess as sp
import os

def UnicornMagicGo(n):
    name = random.choice(n[0]) + ' ' + random.choice(n[1])
    return name

def swirlydecorations(s="~",repeat = 90):
    print(s*repeat)

def pullnames():
    print('\tWelcome to the mystical naming grove! \n*cue the most enchanting music you have ever flipping heard*')
    n = [[],[]]
    adj = False
    with open("adjcorns.txt") as f:
        n[0] = [line.rstrip() for line in f]

    with open("ncorns.txt") as f:
        n[1] = [line.rstrip() for line in f]
    return n

def incantation():
    swirlydecorations()
    n = pullnames()
    magicWords = ("please", "abracadabra")
    spell = input('Speak the magic words to discover your true unicorn name. \n')
    spellDataNormalize = spell.lower()
    spellComponents = spellDataNormalize.split()
    name = UnicornMagicGo(n)
    clear_screen = sp.call('clear',shell=True)


    for x in spellComponents:
        if x in magicWords:

            clear_screen
            path = 'images/'
            asciiFile = os.listdir(path)
            index = random.randrange(0, len(asciiFile))
            randomArt = asciiFile[index]
            image = open(path + randomArt,'r')
            open_image = image.read()


            print ('\n\tBehold! You shall be known as... ' + name)
            swirlydecorations()
            print (open_image)
            swirlydecorations()
            print ('\n\tMay your day be as magical as your name, ' + name + '.')

            break
    
    else:
        print ('Hint: Ask nicely')
        incantation()

incantation()


input()

